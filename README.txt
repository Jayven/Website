This repo is intented to run in parallel with Jayven-Games.
This repo is the Node.js server part of the project.

To setup, create a .env file one directory above project.

.env should look like:

	PORT:13245
	NODE_ENV:local
	JDBC:\\asdf\asdf\.afa/afafs


To run, execute either start.sh or start.bat.
This will load the .env variables and start the web server.

For start.bat: variables are held local to command prompt.
For start.sh: variable are held only to node.js < This is preferable