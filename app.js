if (!(['local', 'dev', 'test', 'production'].includes(process.env.NODE_ENV))){
	throw "Invalid environment mode.";
}
if (process.env.NODE_ENV == 'local'){
	process.env.JDBC = 'postgres:rubbish';
	//Scramble conn string for safety
}

/////////////Includes///////////////
const app = require('express')();

const pathmod = require('path');
fs = require('fs');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var sanitizer = require('sanitizer');

var http = require('http');

var cookieParser = require('cookie-parser');
app.use(cookieParser());

const Template = require(__dirname + '/scripts/page.js');
page = new Template('/base.ejs')

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(require('compression')());

const pool = new require('pg').Pool({connectionString: process.env.JDBC});
function jayven(querystring, variables, callback, cb_params){
	pool.connect(function (err, client, done) {
		function DBError(data){
			//If can't connect to db, log error elseware
			if (process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'local'){
				console.log(data);
			}
			else{
				fs.appendFile("Main.log", Date.now() + "\t" + data + "\n");
			}
		}

		if (err) {
			DBError(err);
		}
		else{
			for (i = 0; i < variables.Lengh; i++){
				variables[i] = sanitizer.escape(variables[i]);
			}
			client.query(querystring, variables, function (err, result) {
				done();
				if (err){
					DBError("Failed query-------------------------:\n-------\n" + querystring + "\t" + variables + "\n\n"+ err + "\n\n" + "Result:\n-------\n" + result + "\n\n");
				}
				if (callback){
					callback(cb_params, result, err);
				}
			});
		}
	});
};

///////////Server init//////////////
const PORT = process.env.PORT || 3000;
const SECURE_PORT = process.env.SECURE_PORT || 3443;

var server = http.createServer(app);

server.listen(PORT, function(){
    log(0, 1, "HTTP server started on port " + PORT);
});


if(!(process.env.NODE_ENV == 'local' || process.env.NODE_ENV == 'dev')){
	const HTTPS_ROUTE = process.env.HTTPS_ROUTE || SECURE_PORT;

	var https = require('https');

	var forceSSL = require('express-force-ssl');
	
	var ssl_options = {
		key: fs.readFileSync(__dirname + '/ssl/privkey.pem'),
		cert: fs.readFileSync(__dirname + '/ssl/fullchain.pem')
	}

	var secureServer = https.createServer(ssl_options, app);

	app.set('forceSSLOptions', {
		httpsPort: HTTPS_ROUTE
	});

	secureServer.listen(SECURE_PORT, function(){
		log(0, 1, "HTTPS server started on port " + SECURE_PORT);
	});

	app.use(forceSSL);
}

////////////Functions///////////////

function log(status, subproc, data){
	if (process.env.NODE_ENV == 'local'){
		console.log(status + "\t" + subproc + "\t" + data);
	}
	else{
		jayven('select py_log($1, $2, $3)', [status, subproc, data]);
	}
};

function RefreshGameDocs(){
	gameDocs = {}
	log(0, 1, "Beginning GameDocs Refresh");
	filelist = fs.readdirSync(__dirname + '/../Games');

	for(i = 0; i < filelist.length; i++){
		try{
			file = fs.readFileSync(__dirname + "/../Games/" + filelist[i] + "/doc.json");

			gameDocs[JSON.parse(file).name] = JSON.parse(file);
		}
		catch(err){}
	}
	log(0, 1, "GameDocs Refresh Complete");
	return gameDocs;
}

function grabGameTable(){
	colors = ["green", "blue", "red"]
	log(0, 1, "Beginning GameDocs Refresh");
	gameTable = "";

	filelist = fs.readdirSync(__dirname + '/../Games');

	for(i = 0; i < filelist.length; i++){
		try{
			file = fs.readFileSync(__dirname + "/../Games/" + filelist[i] + "/doc.json");
			gameTable += "<a href=\"/games/" + JSON.parse(file).name + "\">"
			gameTable += "<table class=\"" + colors[Math.floor(Math.random() * 3)] + "\">"
			gameTable += "<tr>"
			gameTable += "<td>"
			gameTable += JSON.parse(file).name
			gameTable += "</td>"
			gameTable += "</tr>"
			gameTable += "<tr>"
			gameTable += "<td>"
			gameTable += "<img src=\"/gameFiles/" + JSON.parse(file).folder + "/" + "thumb" + "\"/>"
			gameTable += "</td>"
			gameTable += "</tr>"
			gameTable += "</table>"
			gameTable += "</a>"
		}
		catch(err){};
	}
	return gameTable;
}

function grabMenu(Games){
	Tags = {};
	for( i = 0; i < Object.keys(Games).length; i++){
		for (k = 0; k < Games[Object.keys(Games)[i]]["tags"].length; k++){
			if (!(Games[Object.keys(Games)[i]]["tags"][k] in Tags)){
				Tags[Games[Object.keys(Games)[i]]["tags"][k]] = [Games[Object.keys(Games)[i]]["name"]];
			}
			else{
				Tags[Games[Object.keys(Games)[i]]["tags"][k]].push(Games[Object.keys(Games)[i]]["name"]);
			};
		};
	};
	HTML = ""
	HTML += "<li>";
	HTML += "<a href=\"/\">Home</a>"
	HTML += "</li>";
	for(i = 0; i < Object.keys(Tags).length; i++){
		HTML += "<li>";
		HTML += Object.keys(Tags)[i];
		HTML += "<ul>";
		for(k = 0; k < Tags[Object.keys(Tags)[i]].length; k++){
			HTML += "<li>";
			HTML += "<a href=\"\\games\\" + Tags[Object.keys(Tags)[i]][k] + "\">" + Tags[Object.keys(Tags)[i]][k] + "</a>";
			HTML += "</li>";
		}
		HTML += "</ul>";
		HTML += "</li>";
	};
	return HTML; 
};

function clearOldSessions(){
        log(0, 1, "Beginning Pruning Sessions");
        jayven("select * from py_session_prune()", []);
        log(0, 1, "Ending Pruning Sessions");
}

function reload(){
	log(0, 1, "Reload beginning");
	
	clearOldSessions();

	GameDocs = RefreshGameDocs();
	GameTable_HTML = grabGameTable();
	Menu = grabMenu(GameDocs);

	page.default('navigation', Menu);
	page.default('gameTable', GameTable_HTML);

	setTimeout(reload, 1800000);
	log(0, 1, "Reload ending");
}
/////////////Server Start////////////
reload();

/////////////Routing/////////////////
function throwReq(req, res){
	res.status(404);
	res.send("URL Invalid - Are you lost?");
};

function log_req(status, session, url){
	if (process.env.NODE_ENV == 'local'){
		console.log(status + "\t" + session + "\t" + url);
	}
	else{
		jayven('select py_log_request($1, $2, $3)', [status, session, url]);
	}
}

app.get('/ping', function(req, res){
	if (process.env.NODE_ENV != 'local'){
		jayven("select * from py_session_update($1)", [req.cookies.sessionid]);
	}
	res.send("ily");
})

/*#region usermanagement*/
app.post('/login', function(req, res){
	log(0, 1, "Login Attempt: Username:" + req.body.username);
	if (process.env.NODE_ENV != 'local'){
		jayven("select * from py_user_check($1, $2, $3)", [req.headers.username, req.headers.password, req.cookies.sessionid], respondToLogin, [req, res]);
	}
	else{
		res.send("local env");
	}
})
function respondToLogin(cb_params, result){
	try{
		if (result.rows[0].py_user_check){
			cb_params[1].cookie('username', cb_params[0].headers.username, {});
		}
		cb_params[1].send(result.rows[0].py_user_check);
	}
	catch (e){
		log(3, 1, "Create profile attempt failed: unknown reason");
		cb_params[1].send("Unknown Error");
	}
}

app.post('/createprofile', function(req, res){
	log(0, 1, "Create profile Attempt: Username: " + req.headers.newusername + "Email: " + req.headers.newemail);

	if (req.headers.newpassword.length < 1){
		res.send("Password cannot be empty.");
	}
	else{
		if (process.env.NODE_ENV != 'local'){
			jayven("select * from py_user_new($1, $2, $3)", [req.headers.newusername, req.headers.newpassword, req.headers.newemail], respondToCreateProfile, [req, res]);
		}
		else{
			res.send("local env");
		}
	}
})
function respondToCreateProfile(cb_params, result){
	cb_params[1].send(result.rows[0].py_user_new);
}
/*#endregion usermanagement*/

app.post('/getUsername', function(req, res){
	if(req.cookies.sessionid){
		if(process.env.NODE_ENV == 'local'){
			res.send("A free username!");
		}
		else{
			jayven("select * from py_get_username($1)", [req.cookies.sessionid], respondToGetUsername, [req, res]);
		
		}
	}
});

function respondToGetUsername(cb_params, result){
	if(result.rows[0].py_get_username != null){
		cb_params[1].send(result.rows[0].py_get_username);
	}
	else{
		cb_params[1].send("Anon-" + Math.round(Math.random() * 1000));
	}
}

app.all('/*', function(req, res){
	//Manage and then drop robots.txt requests
	if (URL = req.params['0'] == 'robots.txt'){
		res.sendFile(pathmod.join(__dirname, '/webAssets/robots.txt'));
	}

	//Check if it's in rootAssets first(This directory should only be used for updating ssl)
	else if (req.params['0'] != '' && fs.existsSync(pathmod.join(__dirname, '/rootAssets/', req.params['0']))){
        	res.sendFile(pathmod.join(__dirname, '/rootAssets/', req.params['0']))
        }
        	
	//if user didn't send a session
	else if (!req.cookies.sessionid){
		//create new session in database, and goto callback
		if (process.env.NODE_ENV == 'local'){
			result = {};
			result.rows = [{}];
			result.rows[0].py_session_create = "12341234";
			respondToNewSession([req, res], result);
		}
		else{
			jayven("select * from py_session_create($1, $2)", [null, req.connection.remoteAddress], respondToNewSession, [req, res]);
		}
	}
	else{
		respond(req, res);
	}
});

function respondToNewSession(cb_params, result){
	//create sessionid cookie on client
	//res
	cb_params[1].cookie('sessionid', result.rows[0].py_session_create, {});

	//force cookie variable to match as if client sent it
	//this is okay because we just told the client to create the cookie
	//it just doesn't update on this request
	cb_params[0].cookies.sessionid = result.rows[0].py_session_create;

	//continue in routing with sessionid cookie properly loaded
	//same as if the user sent it originally
	respond(cb_params[0], cb_params[1]);
}

function respond(req, res){
	found = false;
	URL = req.params['0'].split('$')[0].split('/');
	if (req.params['0'].split('$')[1]){
		PARAMS = req.params['0'].split('$')[1].split(',');
	}
	else{
		PARAMS = [];
	}

	switch(URL[0]){
		case '':
		case 'home':
			res.send(page.load('home', function(){

				page.ingest('home', '/home.ejs')

				page.plug('home', page.raws['base'], page.raws['home']);

				page.render('home', page.templates['home']);
			}));

			found = true;
			break;
		case 'favicon.ico':
			res.sendFile(__dirname + '/webAssets/favicon.ico');
			found = true;
			break;
		case 'web':
			path = __dirname + '/webAssets/' + URL.slice(1).join('/');
			if (fs.existsSync(path)){
				res.sendFile(path)
				found = true;
			}
			else{
				throwReq(req, res);
			}
			break;
		case 'gameFiles':
			path = pathmod.join(__dirname, '../Games/', URL.slice(1).join('/'));
			if (fs.existsSync(path)){
				res.sendFile(path)
				found = true;
			}
			else{
				throwReq(req, res);
			}
			break;
		case 'games':
			if(GameDocs.hasOwnProperty(URL.slice(1).join('/'))){

				page.ingest('U2017.3', '/Unity-2017.3.ejs')

				page.plug('U2017.3raw', page.raws['base'], page.raws['U2017.3']);
		
				page.plug(URL.slice(1).join('/'), page.templates['U2017.3raw'], {
					pagetitle: [
						{null: GameDocs[URL.slice(1).join('/')]['title']}
					],
					gamesrc: [
						{null: "/gameFiles/" + GameDocs[URL.slice(1).join('/')]['folder'] + "/" + GameDocs[URL.slice(1).join('/')]['source']}
					],
					unityLoader: [
						{null: "/gameFiles/" + GameDocs[URL.slice(1).join('/')]['folder'] + "/" + GameDocs[URL.slice(1).join('/')]['UnityLoader']}
					],
					resx: [
						{null: GameDocs[URL.slice(1).join('/')]['resolution']['x']}
					],
					resy: [
						{null: GameDocs[URL.slice(1).join('/')]['resolution']['y']}
					]
				})
						
				page.render(URL.slice(1).join('/'), page.templates[URL.slice(1).join('/')]);

				res.send(page.load(URL.slice(1).join('/')))
	
				found = true;
				break;
			}
			else{
				throwReq(req, res);
			}
			break;		
		default:
			throwReq(req, res);
			break;
	}

	if (found){
		log_req(7, req.cookies.sessionid, req.params['0']);
	}
	else{
		log_req(6, req.cookies.sessionid, req.params['0']);
	}
};
