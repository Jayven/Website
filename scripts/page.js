//This class loads webpage templates, renders them, and holds them in memory.
//The load function checks if page is in memory before performing file IO.
//When the website becomes to large, this model must change, however, this method will last for a while.
class Templates {
	constructor(filename) {
		this.raws = {};
		this.templates = {};
		this.pages = {};
		this.defaults = {};
		this.ingest('base',filename);		
	}

	//These rules will be applied to everysingle page when there are gaps in rendering
	default(defaultname, value){
		this.defaults[defaultname] = value
	}

	//Callback to load page, then return it
	load(pagename, cb, template){
		//If doesn't exist, follow the callback (which should load it)
		if (!(pagename in this.pages)){
			cb();
		}
		//return it
		if (template){
			return this.render(null, this.plug(null, this.templates[pagename], template));
		}
		else{
			return this.pages[pagename];
		}
	}

	//This function loads the file into memory, and holds it there
	ingest(templatename, filename){

		//Load template to variable
		var new_template = fs.readFileSync(__dirname + '/../views' + filename, 'utf8')

		//This regex isolates the template into a hash of format {DOM: ~~~~}
		var re = /<%%(\w*?)%%>([\S\s]*?)(?=(?:<%%\w*?%%>)|$)/g;

		//Load file through regex into array
		var arr_new_template = {}
		for (var m;m = re.exec(new_template);){
			arr_new_template[m[1]] = m[2];
		}

		//convert array from {DOM: ~~~~} to {DOM: [{spot1: null}, {null: ~~~}]}
		//This splits the '~~~~' to an array of hashs to retain position and the hash name/key
		var re2 = /<%(?:\w*?)%>/g;
		var re3 = /<%(\w*?)%>/g;


		Object.keys(arr_new_template).forEach(function(key){
			//Creates [{null: ~~~}... parts of list
			var new_list = arr_new_template[key].split(re2);
			new_list.forEach(function(element, i){
				new_list[i] = {null: element};
			})
			
			//Inserts {spot1: null}.. between every element
			var k = 0;
			for (var m ;m = re3.exec(arr_new_template[key]);){
				var tmp1 = {};
				tmp1[m[1]] = null
				new_list.splice(2 * k + 1, 0, tmp1);
				k++;
			}

			arr_new_template[key] = new_list;
		});

		this.raws[templatename] = arr_new_template;
	}

	//This function fills in all gaps and finalizes a template into a single string webpage
	render(pagename, template){
		var local_defaults = this.defaults

		var new_page = '';
		template['DOM'].forEach(function(element){
			if (element[null]){
				new_page+=element[null];
			}
			else{
				if (local_defaults[Object.keys(element)[0]] !== undefined){
					new_page+=local_defaults[Object.keys(element)[0]]
				}
			}
		})
		if (pagename == null){
			return new_page;
		}
		else{
			this.pages[pagename] = new_page;
		}
	}

	//This function takes a template and parameters and plugs them in, storing the new page under the p1 name.
	plug(newtemplatename, template, plugs){
		var new_template = JSON.parse(JSON.stringify(template));
		var new_arguments = JSON.parse(JSON.stringify(plugs));

		//iterate arguments
		Object.keys(new_arguments).forEach(function(argkey){

			//iterate template's arguments, most cases this will only one
			Object.keys(new_template).forEach(function(tempkey){

				//iterate template's argument[i]'s content
				new_template[tempkey].forEach(function(element, i){
					//if argkey is in content hash
					if (argkey in element){

						//delete that conent from array
						new_template[tempkey].splice(i,1);

						//reverse arguments and insert one by one, reverse order into the same i
						//Thus they will be in order after
						new_arguments[argkey].reverse().forEach(function(element){
							new_template[tempkey].splice(i, 0, element)
						})
					}
				})
			})
		});
		
		//If no name is given, return the template.
		if (newtemplatename === null){
			return new_template;
		}
		else{
			this.templates[newtemplatename] = new_template;
		}
	}
}


module.exports = Templates;

//Because they get complex and arbitrary, example data models can be found below.


//This is the data model for the 'base template'.
// {
// 	DOM: //The DOM key to the root hash mean this will be "plugged" into the DOM of the browser
// 	[
// 		{ null: '\n<!DOCTYPE html>\n<html>\n\t<head>\n\t\t<link rel="stylesheet" href = "/web/style.css" />\n\t\t<meta name="description" content="Website for small games">\n\t\t<meta name="author" content="Beaker Labs"> \n\t\t<title>Beaker Games</title>\n\t\t<meta charset="UTF-8">\n\t\t<meta name="keywords" content="Games, Jayven, Beaker, Game">\n\t\t<META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">\n\t\t<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n\t\t<link rel="icon" href="/web/Iconx16.ico" type="image/gif" sizes="16x16">\n\t\t<link rel="icon" href="/web/Iconx32.ico" type="image/gif" sizes="32x32">\n\t\t<link rel="icon" href="/web/Iconx64.ico" type="image/gif" sizes="64x64">\n\t\t<link rel="icon" href="/web/Iconx256.ico" type="image/gif" sizes="256x256">\n\t\t<meta charset="utf-8">\n\t\t<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n\t\t' },
// 		{ head: null }, //A hash with a key value of !null means that this is a variable to be filled in
// 		{ null: '\n\t</head>\n\t<body>\n\t\t<nav class="centercontent">\n\t\t\t<img class="logo" src="/web/logo.png"/>\n\t\t\t<ul class="navigation">\n\t\t\t\t' },
// 		{ navigation: null },
// 		{ null: '\n\t\t\t</ul>\n\t\t</nav>\n\t\t' },
// 		{ body: null },
// 		{ null: '\n\t</body>\n</html>\n' }
// 	] 
// }
 

//This is the data model for the game page, intended to be plugged into the base template above
// {
// 	head: //The head tag in the root means it will be plugged into the "head" spot of the template applied to
// 		[
// 			{ null: '\n<title>' },
// 			{ pagetitle: null },
// 			{ null: '</title>\n<script src="' },
// 			{ gamesrc: null },
// 			{ null: '"></script>\n<script>\n\t\tvar gameInstance = UnityLoader.instantiate("gameContainer", "' },
// 			{ unityLoader: null },
// 			{ null: '");\n</script>\n\n' }
// 		],
//    body: 
// 		[
// 			{ null: '\n<div class="centercontent">\n\t<div id="gameContainer" style="width: ' },
// 			{ resx: null },
// 			{ null: 'px; height: ' },
// 			{ resy: null },
// 			{ null: 'px; margin: auto"></div>\n</div>\n' }
// 		]
// }	//Tail is not given here. When a hole is found, render will attempt to fill is with a default, then just close it.
